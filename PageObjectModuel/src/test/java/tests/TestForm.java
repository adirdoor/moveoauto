package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Objects.FillingForm;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TestForm {
	
	WebDriver driver;
	@BeforeTest
	public void beforetest()
	{
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.get("https://www.moveo.group/careers");
		driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[title='application form']")));
		

	}
	
	@Test
	public void fillingForm() throws InterruptedException {
		
		FillingForm form=new FillingForm(driver);
		form.enterName();
		Thread.sleep(2000);
		


		form.enterPosition();
		Thread.sleep(1000);

		form.enterAboutUs();
		Thread.sleep(1000);

		form.enterEmail();
		Thread.sleep(1000);

		form.enterKidomet();
		Thread.sleep(1000);

		form.enterCheck_box();
		Thread.sleep(1000);

		form.enterComment();
		Thread.sleep(1000);
		
		form.enterPhoneNum();
		Thread.sleep(1000);

		form.enterCv();
		Thread.sleep(5000);

		
	}
	
	@AfterTest
	public void AfterTest() {
		
		driver.quit();
	}
	

}
